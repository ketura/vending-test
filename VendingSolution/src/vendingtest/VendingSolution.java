/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingtest;

import java.util.Map;
import java.util.Scanner;
import vendingsolution.InputModule;
import vendingsolution.AccessDeniedException;
import vendingsolution.InventoryModule;
import vendingsolution.Item;
import vendingsolution.LiteralFactory;
import vendingsolution.Slot;
import vendingsolution.VendingController;

/**
 *
 * @author Christian McCarty
 */
public class VendingSolution 
{

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) 
    {
        VendingController vc = LiteralFactory.GetStandardVendingSolution();
        Scanner scanner = new Scanner (System.in);
        String input = "";
        
        PrintPrelude();
        boolean breakout = false;
        
        while(!breakout)
        {
            
            MainMenu();
            boolean authorized = vc.TechAccess();
            if(authorized)
                SecretMenu();
            
            input = scanner.next();
            System.out.println();
            
            switch(input)
            {
                case "1":
                    ReadMenu(vc);
                    break;
                case "2":
                    ReadDisplay(vc);
                    break;
                case "3":
                    InsertCash(vc);
                    break;
                case "4":
                    ReturnCash(vc);
                    break;
                case "5":
                    PressButton(vc);
                    break;
                case "6":
                    SearchTray(vc);
                    break;
                case "7":
                    Goodbye();
                    breakout = true;
                    break;
                default:
                    System.out.println("I didn't catch that.  Please watch your sausage fingers and type carefully.");
            }
        }
    }
    
    public static void PrintPrelude()
    {
        System.out.println("Welcome to the Aperture Science Modularized Virtual Vending Solution!");
        System.out.println("We would like to help you get served today.  What would you like to do?");
    }
    
    public static void MainMenu()
    {
        System.out.println("\n\tMain Menu:");
        System.out.println("\t1. View the Vending Solution menu");
        System.out.println("\t2. Read the display");
        System.out.println("\t3. Insert money");
        System.out.println("\t4. Retrieve money");
        System.out.println("\t5. Press a button");
        System.out.println("\t6. Search for dispensed items");
        System.out.println("\t7. Leave\n");
    }
    
    public static void SecretMenu()
    {
        
    }
    
    public static void PressButton(VendingController vc)
    {
        Scanner scanner = new Scanner (System.in);
        boolean breakout = false;
        
        System.out.println("You look at the keypad and see the letters A-I, the numbers 1-9, 'CANCEL' and 'ENTER'");

        for(int i = 0; i < 2; ++i)
        {
            System.out.println("\nWhat button would you like to push?");
            
            String input = scanner.next().toUpperCase();
            
            switch(input)
            {
                case "A":
                case "B":
                case "C":
                case "D":
                case "E":
                case "F":
                case "G":
                case "H":
                case "I":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                case "ENTER":
                case "CANCEL":
                    vc.PressKey(input);
                    ReadDisplay(vc);
                    break;
                default:
                    System.out.println("\nI'm sorry, I didn't realize we were playing games.  Let me know when you REALLY want to push buttons.");
                    i = 2;
                    break;
            }
                    
        }
    }
    
    public static void SearchTray(VendingController vc)
    {
        System.out.println("You search the tray.");
        System.out.println("You find " + PrettyPrintItemArray(vc.SearchTray()) + "!");
    }
    
    public static void ReturnCash(VendingController vc)
    {
        vc.RetrieveMoney();
        ReadDisplay(vc);
    }
    
    public static void InsertCash(VendingController vc)
    {
        Scanner scanner = new Scanner (System.in);
        boolean breakout = false;
        System.out.println("\nHow much would you like to insert?");
        
        while(!breakout)
        {
            String input = scanner.next();

            double amount = 0.0;
            try
            {
                if(input.toLowerCase().equals("no"))
                {
                    //System.out.println("equality");
                    return;
                }
                amount = Double.parseDouble(input);
                vc.Pay(amount);
                ReadDisplay(vc);
                breakout = true;
            }
            catch(NumberFormatException e)
            {
                System.out.println("\nI didn't quite catch that.  Care to try again?");
            }
        }
    }
    
    public static void Goodbye()
    {
        System.out.println("\nGoodbye!  I'm sure we'll see you again!");
        new Scanner(System.in).next();
    }
    
    public static void ReadMenu(VendingController vc)
    {
        System.out.println("\n\nThis is what you see in the vending solution:\n");
        System.out.println(PrettyPrintMenu(vc.GetMenu()));
    }
    
    public static void ReadDisplay(VendingController vc)
    {
        String result = vc.ReadDisplay();
        if(result.equals(""))
            System.out.println("The display is blank.");
        else
            System.out.println("The display says: " + vc.ReadDisplay());
    }
    
    public static String PrettyPrintMenu(Map<Slot, Item> map)
    {
        String output = "";
        for(Map.Entry<Slot, Item> entry : map.entrySet())
        {
            output += entry.getKey().toString() + " has " + entry.getValue().GetName() + ", ";
        }
        
        output = output.substring(0, output.length() - 2);
                
        return output;
    }
    
    public static String PrettyPrintItemArray(Item[] items)
    {
        String output = "";
        
        if(items.length == 0)
            return "nothing";
        
        for(int i = 0; i < items.length; ++i)
        {
            if(items[i] != null)
                output += items[i].GetName() + ", ";
        }
        
        output = output.substring(0, output.length() - 2);
        
        return output;
    }
    
}
