/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public class UnderhangDispenserModule extends DispenserModule
{
    UnderhangDispenserModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
    }
    
    @Override
    public Item[] SearchTray()
    {
        Item[] tray = Tray.toArray(new Item[Tray.size()]);
        Tray.clear();
        return tray; 
    }
    
    @Override
    void Dispense(Item i)
    {
        Tray.add(i);
    }

}
