/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public class SlotFullException extends Exception
{
    public SlotFullException(String message)
    {
        super(message);
    }
}
