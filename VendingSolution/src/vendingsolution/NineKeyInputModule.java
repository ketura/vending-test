/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public class NineKeyInputModule extends InputModule
{
    NineKeyInputModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
        BufferSize = 2;
    }
    @Override
    public void PressKey(String key)
    {
        if(Buffer.isEmpty())
            parent.display.Clear();
        
        System.out.println("*BEEP*");
                
        if(key.equals(KeyEnter))
            Evaluate();
        else
        {
            parent.display.Display(key);
            Buffer.add(key);
            if(Buffer.size() >= BufferSize)
                Evaluate();
        }
    }
    
    protected void Evaluate()
    {
        String input = "";
        for(String str : Buffer)
            input += str;
        
        if(Buffer.size() <= 0)
            return;
        else if (Buffer.size() == 1)
        {
            switch(Buffer.get(0))
            {
                case Key6:
                    parent.AuthorizeTechAccess();
                    break;
                    
                case KeyCancel:
                    parent.DeauthorizeTechAccess();
                    break;
                case KeyEnter:
                    break;
                    
                default:
                    parent.display.Clear();
                    parent.display.Display("Invalid Input!  Please try again.");
                    break;
            }
            Clear();
        }
        else if(Buffer.size() == 2)
        {
            String row = Buffer.get(0);
            int col;
            try
            {
                col = Integer.parseInt(Buffer.get(1));
            }
            catch(NumberFormatException e)
            {
                col = -1;
            }
            if(!row.matches("[a-zA-Z]") || col < 0)
            {
                Clear();
                parent.display.Display("Invalid Input!  Please try again.");
            }
            else
                parent.money.PerformTransaction(new Slot(row,col));
        }
        Clear();
    }
    
    private void Clear()
    {
        Buffer.clear();
    }
}
