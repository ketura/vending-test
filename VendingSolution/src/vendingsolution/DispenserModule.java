/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;
import java.util.ArrayList;

/**
 *
 * @author Christian McCarty
 */
public abstract class DispenserModule extends Module
{
    protected boolean Locked;
    protected ArrayList<Item> Tray;
    
    protected DispenserModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
        Tray = new ArrayList<>();
    }
    
    
    public abstract Item[] SearchTray();
    void Lock()
    {
        Locked = true;
    }
    
    void Unlock()
    {
        Locked = false;
    }
    
    public void ManualOverride() throws AccessDeniedException
    {
        if(parent.TechAccess())
            Locked = false;
        else
            throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    abstract void Dispense(Item i);
}
