/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public abstract class Module 
{
    protected VendingController parent;
    protected String name;
    
    protected Module(VendingController vc) throws AccessDeniedException
    {
        Install(vc);
    }
    protected void Install(VendingController vc) throws AccessDeniedException
    {
        if(vc.TechAccess())
            parent = vc;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);  
    }
}
