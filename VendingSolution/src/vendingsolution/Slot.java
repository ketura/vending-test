/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public class Slot 
{
    public final String col;
    public final int row;
    
    public Slot(String str, int i)
    {
        this.col = str;
        this.row = i;
    }
    
    public Slot(int x, int y)
    {
        this.col = NumToLetter(x);
        this.row = y;
    }
    
    public int numCol()
    {
        return LetterToNum(col);
    }
    
    public static String NumToLetter(int i)
    {
        if(i >= 1 && i <= 26)
        {
            char c = (char)(i+64);
            return "" + c;
        }
        else return "$";
    }
    
    public static int LetterToNum(String s)
    {
        if(s.length() > 1)
            return -1;
        char c = s.toUpperCase().charAt(0);
        if(c >= 'A' && c <= 'Z')
            return (int)c-64;
        else return -1;
    }
    
    @Override
    public String toString()
    {
        return "" + col + row;
    }
}
