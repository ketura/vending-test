/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public abstract class DisplayModule extends Module
{
    protected String Buffer;
    protected abstract void Display(String out);
    protected void Clear()
    {
        Buffer = "";
    }
    
    public String Read()
    {
        return Buffer;
    }
    
    protected DisplayModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
        Buffer = "";
    }
}
