/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

import java.util.HashMap;
import java.util.Map;
import java.util.LinkedList;
import java.util.ArrayList;

/**
 *
 * @author Christian McCarty
 */
public class GridInventoryModule extends InventoryModule
{
    public final int width;
    public final int height;
    public final int depth;
    protected LinkedList<Item>[][] Items;
    protected Double[][] Prices;
    
    GridInventoryModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
        width = 9;
        height = 9;
        depth = 5;
        
        Items = new LinkedList[width][height];
        Prices = new Double[width][height];
        for(int x=0; x < width; ++x)
        {
            for(int y = 0; y < height; ++y)
            {
                Items[x][y] = new LinkedList<>();
                Prices[x][y] = 0.0;
            }
        }
    }
    
    @Override
    public Map<Slot, Item> GetMenu()
    {
        Map<Slot, Item> map = new HashMap<>();
        for(int x=0; x < width; ++x)
        {
            for(int y = 0; y < height; ++y)
            {
                Item i = Items[x][y].peekFirst();
                if(i != null)
                    map.put(new Slot(x+1, y+1), i);
            }
        }
        return map;
    }
    
    @Override
    public void InsertItem(Item i, Slot s) throws SlotFullException
    {
        LinkedList<Item> slot = Items[s.numCol()-1][s.row-1];
        if(slot.size() >= depth)
            throw new SlotFullException("Slot " + s.toString() + " is full to bursting!");
        else
            slot.addFirst(i);
    }
    
    @Override
    public void FillSlot(Item i, Slot s, int count) throws SlotFullException
    {
        for(int k = 0; k < count; ++k)
        {
            InsertItem(i, s);
        }
    }
    
    @Override
    public Item RemoveItem(Slot s) throws SlotEmptyException
    {
        LinkedList<Item> slot = Items[s.numCol()-1][s.row-1];
        Item i;
        try
        {
            i = slot.remove();
        }
        catch(java.util.NoSuchElementException e)
        {
            i = null;
        }
        if(i == null)
            throw new SlotEmptyException("");
        return i;
    }
    
    @Override
    public Item[] RemoveItems(Slot s, int count) throws SlotEmptyException
    {
        ArrayList<Item> items = new ArrayList<>();
        for(int i = 0; i < count; ++i)
        {
            Item it = RemoveItem(s);
            if(it != null)
                items.add(RemoveItem(s));
        }
        return items.toArray(new Item[count]);
    }
    
    @Override
    public void SetPrice(Slot s, Double cost)
    {
        Prices[s.numCol()-1][s.row-1] = cost;        
    }
    
    @Override
    protected boolean IsEmpty(Slot s)
    {
        return Items[s.numCol()-1][s.row-1].size() == 0;
    }
    
    @Override
    protected boolean IsFull(Slot s)
    {
        return Items[s.numCol()-1][s.row-1].size() >= depth;
    }
    
    @Override
    protected void Dispense(Slot s) throws SlotEmptyException
    {
        parent.dispenser.Unlock();
        parent.dispenser.Dispense(RemoveItem(s));
        parent.dispenser.Lock();
        System.out.println("WHHRRR...KA-CHUNK!");
    }
    
    @Override
    protected Double GetPrice(Slot s)
    {
        return Prices[s.numCol()-1][s.row-1];
    }
}
