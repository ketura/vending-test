/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public class AccessDeniedException extends Exception
{
    public static final String StandardMessage = "You are not authorized to access that!  Shame!";
    public AccessDeniedException(String message)
    {
        super(message);
    }
}
