/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;


/**
 *
 * @author Christian McCarty
 */
public abstract class MoneyModule extends Module
{
    protected double Credit = 0.0;
    protected double Sales = 0.0;
    protected double Till = 0.0;
    protected MoneyModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
    }
    
    public abstract void Pay(double amount);
    public abstract double CashReturn();
    
    public abstract double CountSales()throws AccessDeniedException;
    public abstract double CountTill()throws AccessDeniedException;
    public abstract void ResetSession()throws AccessDeniedException;
    public abstract double RetrieveSales()throws AccessDeniedException;
    public abstract double RetrieveCash()throws AccessDeniedException;
    public abstract void RestockCash(double amount)throws AccessDeniedException;
    
    protected abstract double CheckAmount();
    protected abstract void PerformTransaction(Slot s);
}
