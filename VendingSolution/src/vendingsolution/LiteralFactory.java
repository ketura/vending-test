/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public class LiteralFactory 
{
    public static Item Doritos = new Item("Doritos", 0.75);
    public static Item FieryDoritos = new Item("Fiery Doritos", 0.80);
    public static Item Cheetos = new Item("Cheetos", 0.75);
    public static Item Fritos = new Item("Fritos", 0.60);
    public static Item Lays = new Item("Lays", 0.50);
    public static Item SunChips = new Item("Sun Chips", 1.10);
    public static Item Ruffles = new Item("Ruffles", 0.70);
    public static Item Pringles = new Item("Pringles", 1.50);
    public static Item Cookies = new Item("Cookies", 0.60);
    
    public static Item Sprite = new Item("Sprite", 1.00);
    public static Item MountainDew = new Item("Mountain Dew", 1.00);
    public static Item Pepsi = new Item("Pepsi", 1.00);
    public static Item Coke = new Item("Coke", 1.00);
    public static Item DrPepper = new Item("Dr Pepper", 1.00);
    public static Item Water = new Item("Water", 1.00);
    public static Item DietWater = new Item("Diet Water", 2.00);
    public static Item RootBeer = new Item("Root Beer", 1.00);
    public static Item CreamSoda = new Item("Cream Soda", 1.00);
    
    public static VendingController GetStandardVendingSolution()
    {
        VendingController vc = new VendingController();
        try
        {
            vc.AuthorizeTechAccess();
            vc.InstallInputModule(new NineKeyInputModule(vc));
            vc.InstallDisplayModule(new LCDDisplayModule(vc));
            vc.InstallInventoryModule(new GridInventoryModule(vc));
            StandardStock(vc.inventory);
            vc.InstallDispenserModule(new UnderhangDispenserModule(vc));
            vc.InstallMoneyModule(new CreditCardMoneyModule(vc));
            vc.DeauthorizeTechAccess();
            vc.display.Clear();
            vc.display.Display("How about a snack, friend?");
        }
        catch (AccessDeniedException e)
        {
            System.out.println("The factory exploded!");
        }
        return vc;
    }
    
    public static void StandardStock(InventoryModule inv)
    {
        try
        {
            inv.FillSlot(Doritos, new Slot("A", 1), 5);
            inv.FillSlot(FieryDoritos, new Slot("B", 1), 5);
            inv.FillSlot(Cheetos, new Slot("C", 1), 5);
            inv.FillSlot(Fritos, new Slot("D", 1), 5);
            inv.FillSlot(Lays, new Slot("E", 1), 5);
            inv.FillSlot(SunChips, new Slot("F", 1), 5);
            inv.FillSlot(Ruffles, new Slot("G", 1), 5);
            inv.FillSlot(Pringles, new Slot("H", 1), 5);
            inv.FillSlot(Cookies, new Slot("I", 1), 5);
            
            inv.FillSlot(Sprite, new Slot("A", 2), 5);
            inv.FillSlot(MountainDew, new Slot("B", 2), 5);
            inv.FillSlot(Pepsi, new Slot("C", 2), 5);
            inv.FillSlot(Coke, new Slot("D", 2), 5);
            inv.FillSlot(DrPepper, new Slot("E", 2), 5);
            inv.FillSlot(Water, new Slot("F", 2), 5);
            inv.FillSlot(DietWater, new Slot("G", 2), 5);
            inv.FillSlot(RootBeer, new Slot("H", 2), 5);
            inv.FillSlot(CreamSoda, new Slot("I", 2), 5);
            
            inv.SetPrice(new Slot("A", 1), Doritos.price);
            inv.SetPrice(new Slot("B", 1), FieryDoritos.price);
            inv.SetPrice(new Slot("C", 1), Cheetos.price);
            inv.SetPrice(new Slot("D", 1), Fritos.price);
            inv.SetPrice(new Slot("E", 1), Lays.price);
            inv.SetPrice(new Slot("F", 1), SunChips.price);
            inv.SetPrice(new Slot("G", 1), Ruffles.price);
            inv.SetPrice(new Slot("H", 1), Pringles.price);
            inv.SetPrice(new Slot("I", 1), Cookies.price);

            inv.SetPrice(new Slot("A", 2), Sprite.price);
            inv.SetPrice(new Slot("B", 2), MountainDew.price);
            inv.SetPrice(new Slot("C", 2), Pepsi.price);
            inv.SetPrice(new Slot("D", 2), Coke.price);
            inv.SetPrice(new Slot("E", 2), DrPepper.price);
            inv.SetPrice(new Slot("F", 2), Water.price);
            inv.SetPrice(new Slot("G", 2), DietWater.price);
            inv.SetPrice(new Slot("H", 2), RootBeer.price);
            inv.SetPrice(new Slot("I", 2), CreamSoda.price);
        }
        catch(SlotFullException e)
        {
            System.out.println("The vending machine exploded!");
        }
    }
}
