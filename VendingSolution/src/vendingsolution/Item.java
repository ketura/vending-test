/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author Christian McCarty
 */
public class Item 
{    
    final String name;
    Double price;
    
    public Item(String name, Double price)
    {
        this.name = name;
        this.price = price;
    }
    
    public String GetName()
    {
        return name;
    }
    
    public Double GetPrice()
    {
        return price;
    }
}
