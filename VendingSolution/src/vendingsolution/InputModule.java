/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

import java.util.ArrayList;

/**
 *
 * @author Christian McCarty
 */
public abstract class InputModule extends Module
{
    public static final String Key1 = "1";
    public static final String Key2 = "2";
    public static final String Key3 = "3";
    public static final String Key4 = "4";
    public static final String Key5 = "5";
    public static final String Key6 = "6";
    public static final String Key7 = "7";
    public static final String Key8 = "8";
    public static final String Key9 = "9";
    public static final String KeyCancel = "CANCEL";
    public static final String KeyEnter = "ENTER";
    
    protected int BufferSize;
    protected ArrayList<String> Buffer;
    
    public abstract void PressKey(String button);
    
    protected InputModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
        Buffer = new ArrayList<>();
    }
}
