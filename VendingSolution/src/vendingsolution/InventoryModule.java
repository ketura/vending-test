/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;
import java.util.Map;

/**
 *
 * @author Christian McCarty
 */
public abstract class InventoryModule extends Module
{
    public abstract Map<Slot, Item> GetMenu();
    public abstract void InsertItem(Item i, Slot s) throws SlotFullException; 
    public abstract void FillSlot(Item i, Slot s, int count) throws SlotFullException;
    public abstract Item RemoveItem(Slot s) throws SlotEmptyException;
    public abstract Item[] RemoveItems(Slot s, int count) throws SlotEmptyException;
    public abstract void SetPrice(Slot s, Double cost);
    
    protected abstract boolean IsEmpty(Slot s);
    protected abstract boolean IsFull(Slot s);
    protected abstract void Dispense(Slot s) throws SlotEmptyException;;
    protected abstract Double GetPrice(Slot s);
    
    protected InventoryModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
    }
}
