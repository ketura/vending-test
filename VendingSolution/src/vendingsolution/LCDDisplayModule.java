/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

/**
 *
 * @author ketura
 */
public class LCDDisplayModule extends DisplayModule
{
    LCDDisplayModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
    }
    
    @Override
    protected void Display(String out)
    {
        Buffer += out;
    }
}
