/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;
import java.util.Map;


/**
 *
 * @author Christian McCarty    
 */
public class VendingController 
{
    DisplayModule display;
    InputModule input;
    MoneyModule money;
    InventoryModule inventory;
    DispenserModule dispenser;
    
    private boolean OpenAccess;

    public VendingController()
    {
        OpenAccess = true;
    }
    
    public Map<Slot,Item> GetMenu()
    {
        return inventory.GetMenu();
    }
    
    public void Pay(double amount)
    {
        money.Pay(amount);
    }
    
    public double RetrieveMoney()
    {
        return money.CashReturn();
    }
    
    public void PressKey(String name)
    {
        input.PressKey(name);
    }
    
    public Item[] SearchTray()
    {
        return dispenser.SearchTray();
    }
    
    public String ReadDisplay()
    {
        return display.Read();
    }
    
    void InstallDisplayModule(DisplayModule dm) throws AccessDeniedException
    {
        if(OpenAccess)
            display = dm;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    void InstallInputModule(InputModule im) throws AccessDeniedException
    {
        if(OpenAccess)
            input = im;

        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    void InstallDispenserModule(DispenserModule dm) throws AccessDeniedException
    {
        if(OpenAccess)
            dispenser = dm;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    void InstallMoneyModule(MoneyModule mm) throws AccessDeniedException
    {
        if(OpenAccess)
            money = mm;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    void InstallInventoryModule(InventoryModule im) throws AccessDeniedException
    {
        if(OpenAccess)
            inventory = im;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);

    }
    
    public DisplayModule GetDisplayModule()  throws AccessDeniedException
    {
        if(OpenAccess)
            return display;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }

    public InputModule GetInputModule() throws AccessDeniedException
    {
        if(OpenAccess)
            return input;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }

    public DispenserModule GetDispenserModule() throws AccessDeniedException
    {
        if(OpenAccess)
            return dispenser;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }

    MoneyModule GetMoneyModule() throws AccessDeniedException
    {
        if(OpenAccess)
            return money;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }

    public InventoryModule GetInventoryModule() throws AccessDeniedException
    {
        if(OpenAccess)
            return inventory;
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    protected void AuthorizeTechAccess()
    {
        OpenAccess = true;
        if(display != null)
        {
            display.Clear();
            display.Display("Access granted!");
        }
    }
    
    protected void DeauthorizeTechAccess()
    {
        OpenAccess = false;
        if(display != null)
        {
            display.Clear();
            display.Display("Access rescinded!");
        }
    }
    
    public boolean TechAccess()
    {
        return OpenAccess;
    }
}
