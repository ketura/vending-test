/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingsolution;

import java.text.NumberFormat;

/**
 *
 * @author Christian McCarty
 */
public class CreditCardMoneyModule extends MoneyModule
{
    CreditCardMoneyModule(VendingController vc) throws AccessDeniedException
    {
        super(vc);
    }
    
    @Override
    public void Pay(double amount)
    {
        if(amount > 0)
            Credit += amount;
        
        parent.display.Clear();
        parent.display.Display("Current amount: " + NumberFormat.getCurrencyInstance().format(Credit) + ".");
    }
    
    @Override
    public double CashReturn()
    {
        double owed = Credit;
        Credit = 0.0;
        parent.display.Clear();
        parent.display.Display("Returning $" + owed + ". ");
        parent.display.Display("This is a credit card-only module.  Please don't try to be funny.");
        return owed;
    }
    
    @Override
    public double CountSales()throws AccessDeniedException
    {
        if(parent.TechAccess())
        {
            return Sales;
        }
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    @Override
    public double CountTill()throws AccessDeniedException
    {
        if(parent.TechAccess())
        {
            parent.display.Display("This is a credit card system!  There's nothing in the till.");
            return 0.0;
        }
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    @Override
    public void ResetSession()throws AccessDeniedException
    {
        if (parent.TechAccess()) 
        {
            Sales = 0.0;
        } 
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    @Override
    public double RetrieveSales()throws AccessDeniedException
    {
        if (parent.TechAccess()) 
        {
            parent.display.Display("This is a credit card system!  There's nothing to retrieve.");
            return 0.0;
        } 
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    @Override
    public double RetrieveCash()throws AccessDeniedException
    {
        if (parent.TechAccess()) 
        {
            parent.display.Display("This is a credit card system!  There's nothing to retrieve.");
            return 0.0;
        } 
        else throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    @Override
    public void RestockCash(double amount)throws AccessDeniedException
    {
        if (parent.TechAccess()) 
        {
            parent.display.Display("This is a credit card system!  There's no place for the cash.");
        } 
        else  throw new AccessDeniedException(AccessDeniedException.StandardMessage);
    }
    
    @Override
    protected double CheckAmount()
    {
        return Credit;
    }
    
    @Override
    protected void PerformTransaction(Slot s)       
    {
        Double price = parent.inventory.GetPrice(s);
        if(price > Credit)
        {
            parent.display.Clear();
            parent.display.Display("Insufficient funds! Please insert more to continue.");
        }
        else
        {
            try
            {
                parent.inventory.Dispense(s);
                Credit -= price;
                parent.display.Clear();
                parent.display.Display("Enjoy! " + NumberFormat.getCurrencyInstance().format(Credit) + " remaining.");
            }
            catch(SlotEmptyException e)
            {
                parent.display.Clear();
                parent.display.Display("That item is all out! Please select a different item for purchase.");
            }
        }
    }
}
